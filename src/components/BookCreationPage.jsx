import React from "react";
import { Link } from 'react-router-dom'

const URL = "http://localhost:8080/books";

class BookCreationPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      author: ''
    };

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleAuthorChange = this.handleAuthorChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange(event) {
    this.setState({
      title: event.target.value
    })
  }

  handleAuthorChange(event) {
    this.setState({
      author: event.target.value
    })
  }

  handleSubmit() {
    fetch(URL, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: this.state.title,
        author: this.state.author
      })
    }).then(
      this.props.history.push("/booklist")
    )
  }

  render() {
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="title">Title:</label>
            <input type="text" className="form-control" name="title" value={this.state.title} onChange={this.handleTitleChange}/>
          </div>
          <div className="form-group">
            <label htmlFor="author">Author:</label>
            <input type="text" className="form-control" name="author" value={this.state.author} onChange={this.handleAuthorChange}/>
          </div>
          <input type="submit" value="Add" className="btn btn-primary" />
        </form>

        <footer>
          <div className="text-center">
            <Link to="/booklist">Go to book list >></Link>
          </div>
        </footer>
      </div>
    );
  }
}

export default BookCreationPage;