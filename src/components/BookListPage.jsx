import React from "react";
import { Link } from 'react-router-dom';
import axios from "axios";

const BOOKS_URL = 'http://localhost:8080/books';

class BookListPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      books: [],
      title: '',
      author: ''
    };

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleAuthorChange = this.handleAuthorChange.bind(this);
  }

  handleTitleChange(event) {
    const newTitle = event.target.value;

    axios.get(`${BOOKS_URL}?title=${newTitle}&author=${this.state.author}`
    ).then(
      response => {
        this.setState({
          books: response.data,
          title: newTitle})
      });
  }

  handleAuthorChange(event) {
    const newAuthor = event.target.value;

    axios.get(`${BOOKS_URL}?title=${this.state.title}&author=${newAuthor}`
    ).then(
      response => {
        this.setState({
          books: response.data,
          author: newAuthor})
      });
  }

  componentDidMount() {
    axios.get(BOOKS_URL).then(
      response => {
        this.setState({ books: response.data })
      });
  }

  render() {

    return (
      <div className="container">
        <div className="form-group row">
          <div className="col-md-5">
            <label htmlFor="title">Title:</label>
            <input type="text" className="form-control" name="title" value={this.state.title} onChange={this.handleTitleChange}/>
          </div>
          <div className="col-md-4">
            <label htmlFor="author">Author:</label>
            <input type="text" className="form-control" name="author" value={this.state.author} onChange={this.handleAuthorChange}/>
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th className="col-md-5">Title</th>
              <th className="col-md-4">Author</th>
              <th className="col-md-3">Action</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.books.map(book => {
                return (
                  <tr key={book.id}>
                    <td>{book.title}</td>
                    <td>{book.author}</td>
                    <td>
                      <div>
                        <Link to={`/books/${book.id}/edition-creation`} className="btn btn-default">add edition</Link>
                        <Link to={`/books/${book.id}/editions`} className="btn btn-default">show editions</Link>
                      </div>
                    </td>
                  </tr>)
              })
            }
          </tbody>
        </table>

        <footer>
          <div className="text-center">
            <Link to="/book-creation">Add new book >></Link>
          </div>
        </footer>
      </div>
    );
  }
}

export default BookListPage;