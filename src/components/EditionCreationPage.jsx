import React from "react";
import { Link } from 'react-router-dom'

const URL = "http://localhost:8080/books";

class EditionCreationPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isbn: '',
      quantity: ''
    };

    this.handleIsbnChange = this.handleIsbnChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleIsbnChange(event) {
    this.setState({
      isbn: event.target.value
    })
  }

  handleQuantityChange(event) {
    this.setState({
      quantity: event.target.value
    })
  }

  handleSubmit() {
    fetch(`${URL}/${this.props.match.params.bookId}/editions`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isbn: this.state.isbn,
        quantity: this.state.quantity
      })
    }).then(
      this.props.history.push(`/books/${this.props.match.params.bookId}/editions`)
    )
  }

  render() {
    return (
        <div className="container">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="isbn">ISBN:</label>
              <input type="text" className="form-control" name="isbn" value={this.state.isbn} onChange={this.handleIsbnChange}/>
            </div>
            <div className="form-group">
              <label htmlFor="quantity">Quantity:</label>
              <input type="text" className="form-control" name="quantity" value={this.state.quantity} onChange={this.handleQuantityChange}/>
            </div>
            <input type="submit" value="Add" className="btn btn-primary" />
          </form>
        <footer>
          <div className="text-center">
            <Link to="/booklist">Go to book list >></Link>
          </div>
        </footer>
      </div>
    );
  }
}

export default EditionCreationPage;