import React from "react";
import { Link } from 'react-router-dom'

const BOOKS_URL = 'http://localhost:8080/books';

class EditionListPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      editions: []
    }
  }

  componentDidMount() {
    fetch(`${BOOKS_URL}/${this.props.match.params.bookId}/editions`, {
      method: "GET"
    })
    .then(response => response.json())
    .then(json => {
      this.setState({ editions: json })
    });
  }

  render() {

    return (
      <div className="container">
        <table className="table">
          <thead>
          <tr>
            <th>ISBN</th>
            <th>quantity</th>
            <th>on loan</th>
          </tr>
          </thead>
          <tbody>
          {
            this.state.editions.map(edition => {
              return (
                <tr key={edition.id}>
                  <td>{edition.isbn}</td>
                  <td>{edition.quantity}</td>
                  <td>{edition.onLoan}</td>
                </tr>)
            })
          }
          </tbody>
        </table>

        <footer>
          <div className="text-center">
            <Link to="/booklist">Go to book list >></Link>
          </div>
        </footer>
      </div>
    );
  }
}

export default EditionListPage;