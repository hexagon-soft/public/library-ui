import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import BookListPage from './components/BookListPage';
import BookCreationPage from './components/BookCreationPage';
import EditionListPage from './components/EditionListPage';
import EditionCreationPage from './components/EditionCreationPage';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const App = () => {

  return (
      <BrowserRouter>
        <div>
          <div className="jumbotron text-center">
            <h3>Library</h3>
          </div>
          <Route path="/booklist" component={BookListPage} />
          <Route path="/book-creation" component={BookCreationPage} />
          <Route path="/books/:bookId/editions" component={EditionListPage} />
          <Route path="/books/:bookId/edition-creation" component={EditionCreationPage} />
        </div>
      </BrowserRouter>
  )
};

ReactDOM.render(<App/>, document.getElementById('root'));